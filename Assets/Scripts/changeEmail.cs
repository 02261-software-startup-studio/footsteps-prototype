﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class changeEmail : MonoBehaviour
{
    public Button Submit;
    public InputField emailInputField;
    public Text email;
    // Start is called before the first frame update
    void Start()
    {
        GameObject emailg = GameObject.Find("Email");
        GameObject eg = GameObject.Find("emailInputField");
        Submit = GetComponent<Button>();
        emailInputField = eg.GetComponent<InputField>();
        email = emailg.GetComponent<Text>();
        Submit.onClick.AddListener(changeEmailOnClick);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void changeEmailOnClick()
    {
        if (emailInputField.text == "")
        {
            return;
        }
        email.text = emailInputField.text;
        emailInputField.text = "";
    }
}
