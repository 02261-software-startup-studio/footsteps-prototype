﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class changeDesc : MonoBehaviour
{
	public Button Submit;
	public InputField inputField;
	public TextMeshProUGUI Desc;

	// Start is called before the first frame update
	void Start()
    {
		GameObject descg = GameObject.Find("Desc");
		GameObject ig = GameObject.Find("InputField");
		Submit = GetComponent<Button>();
		inputField = ig.GetComponent<InputField>();
		Desc = descg.GetComponent<TextMeshProUGUI>();
		Submit.onClick.AddListener(changeText);
	}

    // Update is called once per frame
    void Update()
    {
        
    }

    void changeText()
	{
        if(inputField.text == "")
		{
			return;
		}
		Desc.text = inputField.text;
		inputField.text = "";
	}
}
