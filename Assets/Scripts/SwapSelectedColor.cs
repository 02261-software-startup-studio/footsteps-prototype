﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SwapSelectedColor : MonoBehaviour
{
    public Color selectedColor;
    public Color unselectedColor;
    public TextMeshProUGUI visistedText;
    public TextMeshProUGUI myNotesText;

    public void SwapColors(bool visistedSelected)
    {
        visistedText.color = visistedSelected ? selectedColor : unselectedColor;
        myNotesText.color = !visistedSelected ? selectedColor : unselectedColor;

        visistedSelected = !visistedSelected;
    }
}
