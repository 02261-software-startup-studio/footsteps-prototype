﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Note 
{
    public Note(string noteText, Sprite photo, DateTime createdAt, User createdBy)
    {
        NoteText = noteText;
        Photo = photo;
        CreatedAt = createdAt;
        CreatedBy = createdBy;
    }

    public string NoteText;
    public Sprite Photo;
    public DateTime CreatedAt;
    public User CreatedBy;
}
 