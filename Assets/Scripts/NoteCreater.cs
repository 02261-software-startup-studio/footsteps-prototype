﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NoteCreater : MonoBehaviour
{
    public GameObject pinPrefab;
    public NoteSetup notePanel;

    NotesManager notesManager;
    PanZoom camMenu;
    public TextMeshProUGUI noteTextField;
    [HideInInspector] public Sprite noteImage;

    // Start is called before the first frame update
    void Start()
    {
        TouchScreenKeyboard.hideInput = true;
        notesManager = FindObjectOfType<NotesManager>();

        for(int i = 3; i < notesManager.myNotes.Count; i++)
        {
            CreatePin(notesManager.myNotes[i]);
        }

        this.gameObject.SetActive(false);
    }

    public void CreateNote()
    {
        string noteText = noteTextField.text;
        noteTextField.GetComponentInParent<TMP_InputField>().text = string.Empty;

        Note note = new Note(noteText, noteImage, DateTime.Now, notesManager.activeUser);
        notesManager.myNotes.Add(note);
        this.gameObject.SetActive(false);
        noteImage = null;

        CreatePin(note);
    }

    public void CreatePin(Note note)
    {
        GameObject pinGO = Instantiate(pinPrefab, new Vector3(0, 0.5f, 0), Quaternion.identity);
        pinGO.name = "Clickable Pin";
        ClickablePin pin = pinGO.GetComponent<ClickablePin>();
        pin.note = note;
        pin.notePanel = notePanel;
    }
}
