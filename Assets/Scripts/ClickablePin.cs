﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickablePin : MonoBehaviour
{
    public Note note;
    public NoteSetup notePanel;
    float maxDistance = 1f;
    public GameObject infoPanel;

    private void OnMouseDown()
    {
        if (FindObjectOfType<PanZoom>().menuOpen) return;

        if (Vector3.Distance(this.transform.position, Vector3.zero) < maxDistance)
        {
            notePanel.gameObject.SetActive(true);
            notePanel.Setup(note);
            FindObjectOfType<PanZoom>().OpenMenu();
        }
        else
        {
            StartCoroutine(DisplayHelpMessage());

        }
    }

    IEnumerator DisplayHelpMessage()
    {
        infoPanel.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        infoPanel.gameObject.SetActive(false);
    }
}
