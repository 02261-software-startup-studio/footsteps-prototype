﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    WebCamTexture webCamTexture;

    void Start()
    {
        //webCamTexture = new WebCamTexture();
        // GetComponent<Renderer>().material.mainTexture = webCamTexture; //Add Mesh Renderer to the GameObject to which this script is attached to
        //webCamTexture.Play();
    }

    public void StartTakePhoto()
    {
        //StartCoroutine(TakePhoto());
    }

    IEnumerator TakePhoto()  
    {
        yield return new WaitForEndOfFrame();

        Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
        photo.SetPixels(webCamTexture.GetPixels());
        photo.Apply();
    }
}
