﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NotesManager : MonoBehaviour
{
    public List<Note> myNotes = new List<Note>();
    public Note[] visitedNotes;
    public User activeUser;

    private static NotesManager notesManager;
    void Awake()
    {
        DontDestroyOnLoad(this);

        if (notesManager == null)
        {
            notesManager = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
