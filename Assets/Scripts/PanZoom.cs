﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanZoom : MonoBehaviour
{
    Camera cam;
    Vector3 touchStart;
    public float zoomOutMin = 1;
    public float zoomOutMax = 8;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    public bool menuOpen = false;

    private void Start()
    {
        cam = GetComponent<Camera>();        
    }

    // Update is called once per frame
    void Update()
    {
        if (menuOpen) return;

        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            zoom(difference * 0.01f);
        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            cam.transform.position += direction;
            Vector3 tempPos = cam.transform.position;
            tempPos.x = Mathf.Clamp(tempPos.x, xMin, xMax);
            tempPos.y = Mathf.Clamp(tempPos.y, yMin, yMax);
            cam.transform.position = tempPos;
        }
        zoom(Input.GetAxis("Mouse ScrollWheel"));
    }

    void zoom(float increment)
    {
        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize - increment, zoomOutMin, zoomOutMax);
    }

    public void OpenMenu()
    {
        menuOpen = true;
    }
    public void CloseMenu()
    {
        menuOpen = false;
    }
}