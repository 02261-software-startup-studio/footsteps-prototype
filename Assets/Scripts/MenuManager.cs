﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void LoadMap()
    {
        SceneManager.LoadScene("Map");
    }

    public void LoadNotes()
    {
        SceneManager.LoadScene("Notes");
    }

    public void LoadProfile()
    {
        SceneManager.LoadScene("Profile");
    }
}
