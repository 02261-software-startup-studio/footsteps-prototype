﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollViewPopulator : MonoBehaviour
{
    public GameObject visistedScrollContent;
    public GameObject myNotesScrollContent;
    public GameObject notePrefab;
    NotesManager notesManager;
    
    // Start is called before the first frame update
    void Start()
    {
        notesManager = FindObjectOfType<NotesManager>();
        PopulateView();
    }

    void PopulateView() 
    {
        foreach(var visited in notesManager.visitedNotes)
        {
            GameObject go = Instantiate(notePrefab);
            go.GetComponent<NoteSetup>().Setup(visited);
            go.transform.SetParent(visistedScrollContent.transform, false);
        }

        foreach(var myNotes in notesManager.myNotes)
        {
            GameObject go = Instantiate(notePrefab);
            go.GetComponent<NoteSetup>().Setup(myNotes);
            go.transform.SetParent(myNotesScrollContent.transform, false);
        }
    }
}
