﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NoteSetup : MonoBehaviour
{
    public Image profilePic;
    public Image storyPic;
    public TextMeshProUGUI username;
    public TextMeshProUGUI date;
    public TextMeshProUGUI storyText;

    public void Setup(Note note)
    {
        profilePic.sprite = note.CreatedBy.ProfilePicture;
        storyPic.sprite = note.Photo;
        username.text = note.CreatedBy.Name;
        date.text = note.CreatedAt.ToString("dd-MM/yyyy");
        storyText.text = note.NoteText;
    }   
}
